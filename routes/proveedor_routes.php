<?php
	require_once './controller/proveedor_controller.php';
	$controller = new Proveedor();
	switch($_GET['controller']){
		case 'listar_proveedor_ruta':{
			$controller->listar_proveedor_ruta();
			break;
		}
		case 'listar_proveedor':{
			$controller->listar_proveedor();
			break;
		}
		case 'listar_proveedor_ruta_reg_diario':{
			$request_json = file_get_contents('php://input');
			$request = json_decode($request_json);
			$fecha = $request->fecha;
			$controller->listar_proveedor_ruta_reg_diario($fecha);
			break;
		}
	}
?>
