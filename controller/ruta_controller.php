<?php 
	class Ruta{		
	    private $loader;
	    private $twig;
	    private $model;

		private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
		);

		//private $response;

	    public function __construct(){	    	
			require_once 'model/Ruta.php';
			$this->model = new Rutas();

	    	$this->loader = new \Twig\Loader\FilesystemLoader('views/');
	    	$this->twig = new \Twig\Environment($this->loader, [
	    	 	/*'cache' => 'cache/',
	    		'debug' => true*/
	    	]);
	    }

	    public function index(){	    	
	    	 echo $this->twig->render('ruta/index.twig', ['usuario' => 'Mardxc']);	    	
	    }

	    public function listar_ruta(){
	    	$this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
	    	$this->response["data"] 		= $this->model->listar_ruta();
	    	//$this->response 		= $this->model->listar_ruta();
	    	echo json_encode($this->response);
		}
		
		public function listar_ruta_especifica($id){
			$this->response["status"] 		= "ok";
	    	$this->response["body"] 		= "Registros encontrados";
	    	$this->response["data"] 		= $this->model->listar_ruta_especifica($id);
	    	//$this->response 		= $this->model->listar_ruta();
	    	echo json_encode($this->response);
		}

	}

 ?>