<?php 
	//require_once 'vendor/autoload.php'; // Es el cargador de dependencias de composer
	class Administracion{
		
	    private $loader;
	    private $twig;

	    public function __construct(){	    	
			
	    	$this->loader = new \Twig\Loader\FilesystemLoader('views/'); // Recibe la ruta donde estaran las vistas
	    	$this->twig = new \Twig\Environment($this->loader, [ // Se pasa el loader y las configuraciones adicionales que se pudieran agregar
	    	 	/*'cache' => 'cache/',
	    		'debug' => true*/
	    	]);
	    }

	    public function index(){
	    	// recibe el nombre de la vista, puede ser cualquier extension, ademas recibe las variables para pasar a la vista
	    	 echo $this->twig->render('administracion/index.twig', ['usuario' => 'Mardxc']);	    	
	    }
	}

 ?>