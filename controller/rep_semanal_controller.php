<?php 
	class Rep_semanal{
		
	    private $loader;
		private $twig;
		
		private $reporte = array(
			"lunes" => "",
			"martes" => "",
			"miercoles" => "",
			"jueves" => "",
			"viernes" => "",
			"sabado" => "",
			"domingo" => "",
			"info" => "",
			"num_semana" => ""
		);
		private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
		);
		private $num_semana_actual;

		public $resultado_rutas=[];
	    public function __construct(){	    	
			require_once 'model/Asistencia.php';
			$this->model = new Asistencia();
	    	$this->loader = new \Twig\Loader\FilesystemLoader('views/');
	    	$this->twig = new \Twig\Environment($this->loader, [
	    	 	/*'cache' => 'cache/',
	    		'debug' => true*/
	    	]);
	    }

	    public function index(){
	    	 echo $this->twig->render('reportes/reporte_semanal.twig', ['usuario' => 'Javi']);	    	
		}
		
		public function select_reporte_semanal($num_semana){
			$resultado = $this->model->select_reporte_semanal_fecha($num_semana);
			/*$resultado_id = $this->model->select_reporte_semanal($num_semana);
			require_once 'model/Proveedor.php';
			$this->modelP = new Proveedores();
			$reporte['info'] = $this->modelP->listar_proveedor_ruta();
			for($i=0; $i<count($resultado_id);$i++){
				$resultado_rutas = $this->model->select_reporte_semanal_rutas($resultado_id[$i]['id_asistencia']);
				
				$fecha = $resultado_id[$i]['fecha'];
				$reporte['num_semana'][$i] = date('W',strtotime($fecha));
				switch (date('w',strtotime($fecha))){
					case 0:{
						$reporte['domingo'] = $resultado_rutas;
						break;
					}
					case 1:{
						$reporte['lunes'] = $resultado_rutas;
						break;
					}
					case 2:{
						$reporte['martes'] = $resultado_rutas;
						break;
					}
					case 3:{
						$reporte['miercoles'] = $resultado_rutas;
						break;
					}
					case 4:{
						$reporte['jueves'] = $resultado_rutas;
						break;
					}
					case 5:{
						$reporte['viernes'] = $resultado_rutas;
						break;
					}
					case 6:{
						$reporte['sabado'] = $resultado_rutas;
						break;
					}
				}
			}*/
			//$result_reporte['info']=$info;
			//$result_reporte['dia']=$dias;
			//$this->response 		= $this->model->listar_ruta();
			$this->response["status"] 		= "ok";
			$this->response["body"] 		= "Registros encontrados";
			
			//$this->response["data"] 		= $reporte;
			$this->response["data"] 		= $this->model->select_reporte_semanal_fecha($num_semana);
			echo json_encode($this->response);
		}

		public function get_num_semana_actual(){
			$fecha_actual = getdate();
			$semana_actual = date('W',strtotime("".$fecha_actual['year']."-".$fecha_actual['mon']."-".$fecha_actual['mday']));
			
			$this->num_semana_actual = $semana_actual;
			echo json_encode($this->num_semana_actual);
		}
	}

 ?>