<?php 
	/*require_once 'vendor/autoload.php'; // Es el cargador de dependencias de composer

	$loader = new \Twig\Loader\FilesystemLoader('./views/'); // Recibe la ruta donde estaran las vistas
	$twig = new \Twig\Environment($loader, [ // Se pasa el loader y las configuraciones adicionales que se pudieran agregar
	 //'cache' => '/path/to/compilation_cache',
	]);

	// recibe el nombre de la vista, puede ser cualquier extension, ademas recibe las variables para pasar a la vista
	 echo $twig->render('main.twig', ['usuario' => 'Mardxc']);*/
	 //echo $twig->render('main.html.twig');
  ?>

<?php
	require_once 'vendor/autoload.php'; // Es el cargador de dependencias de composer
	if(!isset($_GET['route'])){
		$loader = new \Twig\Loader\FilesystemLoader('views/'); // Recibe la ruta donde estaran las vistas
		$twig = new \Twig\Environment($loader, [ // Se pasa el loader y las configuraciones adicionales que se pudieran agregar
		 	/*'cache' => 'cache/',
		 	'debug' => true*/
		]);
		// recibe el nombre de la vista, puede ser cualquier extension, ademas recibe las variables para pasar a la vista
		 echo $twig->render('main.twig', ['usuario' => 'Mardxc']);
	}else{
		switch($_GET['route']){
			case 'admin':{
				include_once 'routes/administracion_routes.php';
				break;
			}
			case 'reg_diario':{
				include_once 'routes/reg_diario_routes.php';
				break;				
			}
			case 'reporte':{
				include_once 'routes/rep_semanal_routes.php';
				break;				
			}
			case 'proveedor':{
				include_once 'routes/proveedor_routes.php';
				break;
			}
			case 'ruta':{
				include_once 'routes/ruta_routes.php';
				break;
			}
			default:{
			}
		}
	}
?>
