<?php
	class Proveedores{
		private $db;
		private $result;

		public function __construct() {
			require_once './config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
		}

		public function listar_proveedor_ruta() {
			try {
				$sql= "SELECT
						ru.nombre AS ruta,
						ru.id_ruta,
					    (SELECT pro.nombre FROM proveedor pro WHERE ru.id_proveedor = pro.id_proveedor LIMIT 1) AS nombre,
					    (SELECT pro.id_proveedor FROM proveedor pro WHERE ru.id_proveedor = pro.id_proveedor LIMIT 1) AS id_proveedor,
						(SELECT pro.nombre FROM proveedor pro WHERE ru.id_proveedor = pro.id_proveedor LIMIT 1) AS proveedor
					FROM
						ruta ru
					WHERE ru.status = 1";
				$sql = $this->db->prepare($sql);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}

		public function listar_proveedor_ruta_reg_diario($fecha) {
			try {
				$sql= "SELECT 
						da.id_ruta,
						(SELECT ru.nombre FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) AS ruta,
						(SELECT pro.nombre FROM proveedor pro WHERE pro.id_proveedor = (SELECT ru.id_proveedor FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) LIMIT 1) AS nombre,
						(SELECT pro.id_proveedor FROM proveedor pro WHERE pro.id_proveedor = (SELECT ru.id_proveedor FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) LIMIT 1) AS id_proveedor
					FROM
						det_asistencia da
					WHERE
						da.id_asistencia = (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.fecha = :fecha LIMIT 1)
					AND
						da.regtistrada = 0";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":fecha", 	$fecha, 	PDO::PARAM_STR);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}

		public function validar_rutas_fecha_actual($fecha) {
			try {
				$sql= "SELECT 
						da.id_ruta,
						(SELECT ru.nombre FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) AS ruta,
						(SELECT pro.nombre FROM proveedor pro WHERE pro.id_proveedor = (SELECT ru.id_proveedor FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) LIMIT 1) AS nombre,
						(SELECT pro.id_proveedor FROM proveedor pro WHERE pro.id_proveedor = (SELECT ru.id_proveedor FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) LIMIT 1) AS id_proveedor
					FROM
						det_asistencia da
					WHERE
						da.id_asistencia = (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.fecha = :fecha LIMIT 1)";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":fecha", 	$fecha, 	PDO::PARAM_STR);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}

		public function listar_proveedor_hoy($fecha) {
			try {
				$sql= "SELECT 
					DISTINCT
						(SELECT pro.nombre FROM proveedor pro WHERE pro.id_proveedor = (SELECT ru.id_proveedor FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) LIMIT 1) AS nombre,
						(SELECT pro.id_proveedor FROM proveedor pro WHERE pro.id_proveedor = (SELECT ru.id_proveedor FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) LIMIT 1) AS id_proveedor
					FROM
						det_asistencia da
					WHERE
						da.id_asistencia = (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.fecha = :fecha LIMIT 1)
					AND
						da.regtistrada = 0";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":fecha", 	$fecha, 	PDO::PARAM_STR);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}

		public function listar_proveedor() {
			try {
				$sql= "SELECT
						*
					FROM
						proveedor pro
					WHERE pro.status = 1";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":fecha", 	$fecha, 	PDO::PARAM_STR);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}

	}
 ?>
