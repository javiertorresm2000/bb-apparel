<?php
	date_default_timezone_set("America/Mexico_City");
	class Asistencia{
		private $db;
		private $fecha_server;
		private $fecha_hora_server;
		private $result;

		public function __construct(){
			$this->fecha_hora_server = date("Y-m-d H:i:s");
			$this->fecha_server = date("Y-m-d");
			require_once './config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
		}

		public function guardar_registro_diario($data) {
			try {
				//$id_usuario 	= $_SESSION['id'];
				$id_usuario 	= 1;
				$this->db->beginTransaction();
				// ASISTENCIA
					$cantidad_ordinaria 	= $data['cantidad_ordinaria'];
					$cantidad_extra 	= 0;//$data['cantidad_extra'];
					$fecha 				= $data['fecha'];
					$num_semana 		= date('W',  strtotime($fecha));
					$anio 				= date("Y", strtotime($fecha));
					$num_dia 			= date('N',  strtotime($fecha));

					switch ($num_dia) {
						case '1':{
							$nom_dia = "LUNES";
							break;
						}
						case '2':{
							$nom_dia = "MARTES";
							break;
						}
						case '3':{
							$nom_dia = "MIERCOLES";
							break;
						}
						case '4':{
							$nom_dia = "JUEVES";
							break;
						}
						case '5':{
							$nom_dia = "VIERNES";
							break;
						}
						case '6':{
							$nom_dia = "SABADO";
							break;
						}
						case '7':{
							$nom_dia = "DOMINGO";
							break;
						}
					}

					$sql ="SELECT id_asistencia FROM asistencia WHERE fecha = :fecha LIMIT 1";
					$sql = $this->db->prepare($sql);
					$sql->bindParam(":fecha",	$fecha, PDO::PARAM_STR);
					$sql->execute();
					$resultado = $sql->fetch(PDO::FETCH_ASSOC);	

					$this->result = $sql->fetch(PDO::FETCH_ASSOC);	
					if ($resultado) {
						$id_asistencia = $resultado['id_asistencia'];
					}else{
						$sql="
							INSERT INTO asistencia(
								cantidad_ordinaria,
								cantidad_extra,
								fecha,
								num_semana,
								anio)
							VALUES (
								:cantidad_ordinaria,
								:cantidad_extra,
								:fecha,
								:num_semana,
								:anio)";
						$sql = $this->db->prepare($sql);
						$sql->bindParam(":cantidad_ordinaria", 	$cantidad_ordinaria, 	PDO::PARAM_STR);
						$sql->bindParam(":cantidad_extra", 		$cantidad_extra, 		PDO::PARAM_STR);
						$sql->bindParam(":fecha", 				$fecha,	PDO::PARAM_STR);
						$sql->bindParam(":num_semana", 			$num_semana,			PDO::PARAM_STR);
						$sql->bindParam(":anio", 				$anio,					PDO::PARAM_STR);
						$sql->execute();
						$id_asistencia = $this->db->lastInsertId();
						//
							$sql= "SELECT
									id_ruta
								FROM
									ruta ru
								WHERE 
								 ru.status = 1";
							$sql = $this->db->prepare($sql);
							$sql->execute();

							$id_rutas = $sql->fetchAll(PDO::FETCH_ASSOC);
						//
						for ($i=0; $i < count($id_rutas); $i++) {
							$sql="INSERT INTO det_asistencia (
								id_ruta,
								id_asistencia)
							VALUES (
								:id_ruta,
								:id_asistencia)";
							$sql = $this->db->prepare($sql);
							$sql->bindParam(":id_ruta", 			$id_rutas[$i]['id_ruta'], 	PDO::PARAM_INT);
							$sql->bindParam(":id_asistencia", 		$id_asistencia, 		PDO::PARAM_INT);
							$sql->execute();
						}
					}				
				// COMENTARIOS
					$descripcion 	= $data['descripcion'];
					$sql= "INSERT INTO comentario(
							descripcion,
							fecha,
							id_usuario)
						VALUES (
							:descripcion,
							:fecha,
							:id_usuario)";
					$sql = $this->db->prepare($sql);
					$sql->bindParam(":descripcion", 	$descripcion, 			PDO::PARAM_STR);
					$sql->bindParam(":fecha", 			$this->fecha_server, 	PDO::PARAM_STR);
					$sql->bindParam(":id_usuario", 		$id_usuario, 			PDO::PARAM_INT);
					$sql->execute();
					$id_comentario = $this->db->lastInsertId();
				// COMENTARIOS
				// DET ASISTENCIA
					$personal_recibido 	= $data['personal_recibido'];
					$id_ruta 			= $data['id_ruta'];
					$hora_llegada 		= $data['hora_llegada'];
						//
						$sql= "SELECT
									da.id_det_asistencia
								FROM
									det_asistencia da
								WHERE
									da.id_ruta = :id_ruta
								AND
									da.id_asistencia = :id_asistencia";
							$sql = $this->db->prepare($sql);
							$sql->bindParam(":id_ruta", 		$id_ruta, 		PDO::PARAM_INT);
							$sql->bindParam(":id_asistencia", 	$id_asistencia, PDO::PARAM_INT);

							$sql->execute();

							$id_det_asistencia = $sql->fetch(PDO::FETCH_ASSOC);
						//
					$sql="UPDATE det_asistencia SET 
							fecha               = :fecha,
							personal_recibido   = :personal_recibido,
							id_comentario       = :id_comentario,
							id_usuario          = :id_usuario,
							hora_llegada        = :hora_llegada,
							hora_esperada       = :hora_esperada,
							regtistrada         = '1',
							nom_dia 			= :nom_dia
						WHERE (
							id_det_asistencia = :id_det_asistencia
						)";
					$sql = $this->db->prepare($sql);
					$sql->bindParam(":fecha", 				$this->fecha_server, 	PDO::PARAM_STR);
					$sql->bindParam(":personal_recibido", 	$personal_recibido, 	PDO::PARAM_INT);
					$sql->bindParam(":id_comentario", 		$id_comentario, 		PDO::PARAM_INT);
					$sql->bindParam(":id_usuario", 			$id_usuario, 			PDO::PARAM_INT);
					$sql->bindParam(":hora_llegada", 		$hora_llegada, 			PDO::PARAM_STR);
					$sql->bindParam(":hora_esperada", 		$hora_llegada, 			PDO::PARAM_STR);
					$sql->bindParam(":id_det_asistencia", 	$id_det_asistencia['id_det_asistencia'], 	PDO::PARAM_INT);
					$sql->bindParam(":nom_dia", 			$nom_dia,		PDO::PARAM_STR);

					$sql->execute();
					$this->result = $this->db->lastInsertId();

				$this->db->commit();
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
				$this->db->rollback();
			}

			return $this->result;
		}

		public function guardar_registro_semanal($data) {
			try {
				//$id_usuario 	= $_SESSION['id'];
				$id_usuario 	= 1;
				$this->db->beginTransaction();
				// ASISTENCIA
					$fecha 			= $data['fecha'];
					$num_semana 	= date('W',  strtotime($fecha));
					$anio 			= date("Y", strtotime($fecha));

					$sql ="SELECT id_asistencia FROM asistencia WHERE fecha = :fecha LIMIT 1";
					$sql = $this->db->prepare($sql);
					$sql->bindParam(":fecha",	$fecha, PDO::PARAM_STR);
					$sql->execute();
					$resultado = $sql->fetch(PDO::FETCH_ASSOC);	

					$this->result = $sql->fetch(PDO::FETCH_ASSOC);	
					if ($resultado) {
						$id_asistencia = $resultado['id_asistencia'];
					}else{
						$sql="
							INSERT INTO asistencia(
								fecha,
								num_semana,
								anio)
							VALUES (
								:fecha,
								:num_semana,
								:anio)";
						$sql = $this->db->prepare($sql);
						$sql->bindParam(":fecha", 		$fecha,			PDO::PARAM_STR);
						$sql->bindParam(":num_semana", 	$num_semana,	PDO::PARAM_STR);
						$sql->bindParam(":anio", 		$anio,			PDO::PARAM_STR);
						$sql->execute();
						$id_asistencia = $this->db->lastInsertId();
						//
							$sql= "SELECT
									id_ruta
								FROM
									ruta ru
								WHERE 
								 ru.status = 1";
							$sql = $this->db->prepare($sql);
							$sql->execute();

							$id_rutas = $sql->fetchAll(PDO::FETCH_ASSOC);
						//
						for ($i=0; $i < count($id_rutas); $i++) {
							$sql="INSERT INTO det_asistencia (
								id_ruta,
								id_asistencia)
							VALUES (
								:id_ruta,
								:id_asistencia)";
							$sql = $this->db->prepare($sql);
							$sql->bindParam(":id_ruta", 			$id_rutas[$i]['id_ruta'], 	PDO::PARAM_INT);
							$sql->bindParam(":id_asistencia", 		$id_asistencia, 		PDO::PARAM_INT);
							$sql->execute();
						}
					}
					$this->result = $this->db->lastInsertId();

				$this->db->commit();
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
				$this->db->rollback();
			}

			return $this->result;
		}

		//JAVI
		public function select_reporte_semanal($num_semana){
			try{
				
				$this->db->beginTransaction();
				$sql ="SELECT id_asistencia, fecha FROM asistencia WHERE num_semana = :num_semana";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":num_semana",$num_semana, PDO::PARAM_INT);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);

			}catch (Exception $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function select_reporte_semanal_rutas($id_asistencia){
			try{
				$sql ="SELECT * FROM det_asistencia WHERE id_asistencia = :id_asistencia ORDER BY id_ruta ASC";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":id_asistencia",$id_asistencia, PDO::PARAM_INT);
				$sql->execute();
				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
			}catch( Exception $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		public function select_reporte_semanal_fecha($num_semana){
			try{
				$sql ="SELECT 
					ru.no_ruta AS id_ruta,
					ru.nombre AS ruta,
					(SELECT pro.nombre FROM proveedor pro WHERE pro.id_proveedor = ru.id_proveedor LIMIT 1) AS proveedor,
					#LUNES
					IFNULL((SELECT da.personal_recibido FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='LUNES' ),0) AS lunes_personal_recibido,
					IFNULL((SELECT da.hora_llegada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='LUNES' ),0) AS lunes_hora_llegada,
					IFNULL((SELECT da.hora_esperada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='LUNES' ),0) AS lunes_hora_esperada,
					#MARTES
					IFNULL((SELECT da.personal_recibido FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='MARTES' ),0) AS martes_personal_recibido,
					IFNULL((SELECT da.hora_llegada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='MARTES' ),0) AS martes_hora_llegada,
					IFNULL((SELECT da.hora_esperada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='MARTES' ),0) AS martes_hora_esperada,
					#MIERCOLES
					IFNULL((SELECT da.personal_recibido FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='MIERCOLES' ),0) AS miercoles_personal_recibido,
					IFNULL((SELECT da.hora_llegada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='MIERCOLES' ),0) AS miercoles_hora_llegada,
					IFNULL((SELECT da.hora_esperada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='MIERCOLES' ),0) AS miercoles_hora_esperada,
					#JUEVES
					IFNULL((SELECT da.personal_recibido FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='JUEVES' ),0) AS jueves_personal_recibido,
					IFNULL((SELECT da.hora_llegada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='JUEVES' ),0) AS jueves_hora_llegada,
					IFNULL((SELECT da.hora_esperada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='JUEVES' ),0) AS jueves_hora_esperada,
					#VIERNES
					IFNULL((SELECT da.personal_recibido FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='VIERNES' ),0) AS viernes_personal_recibido,
					IFNULL((SELECT da.hora_llegada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='VIERNES' ),0) AS viernes_hora_llegada,
					IFNULL((SELECT da.hora_esperada FROM det_asistencia da WHERE da.id_ruta = ru.id_ruta AND da.id_asistencia IN (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.num_semana= :num_semana) AND da.nom_dia ='VIERNES' ),0) AS viernes_hora_esperada
				FROM
					ruta ru;";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":num_semana",$num_semana, PDO::PARAM_INT);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);

			}catch (Exception $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}

		//JAVI
		public function get_fecha_asistencia($fecha){
			try{
				$sql = "SELECT
							*
						FROM
							asistencia 
						WHERE
							fecha = $fecha";
				
				$sql = $this->db->prepare($sql);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
			}catch (Exception $e){
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->result;
		}
	}
 ?>
