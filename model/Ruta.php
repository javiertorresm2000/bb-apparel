<?php
	class Rutas{
		private $db;
		private $result;

		public function __construct(){
			require_once './config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
		}

		public function listar_ruta_hoy($fecha) {
			try {
				$sql= "SELECT
						da.id_ruta,
						(SELECT ru.nombre FROM ruta ru WHERE ru.id_ruta = da.id_ruta LIMIT 1) AS nombre
					FROM
						det_asistencia da
					WHERE
						da.id_asistencia = (SELECT asis.id_asistencia FROM asistencia asis WHERE asis.fecha = :fecha LIMIT 1)
					AND
						da.regtistrada = 0";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":fecha", 	$fecha, 	PDO::PARAM_STR);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}

		public function listar_ruta() {
			try {
				$sql= "SELECT
						*
					FROM
						ruta ru
					WHERE ru.status = 1";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":fecha", 	$fecha, 	PDO::PARAM_STR);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}

		public function listar_ruta_especifica($id) {
			try {
				$sql= "SELECT
						*,
						(SELECT capacidad FROM vehiculo WHERE id_vehiculo = ru.id_vehiculo LIMIT 1) AS capacidad
					FROM
						ruta ru
					WHERE ru.id_ruta = $id";
				$sql = $this->db->prepare($sql);
				$sql->bindParam(":fecha", 	$fecha, 	PDO::PARAM_STR);
				$sql->execute();

				$this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
				
			} catch (Exception $e) {
				$this->result = $e->getMessage().' LINEA['.$e->getLine().'].';
			}

			return $this->result;
		}
	}
 ?>
